CC              := g++
CFLAGS          := -std=c++11
LDFLAGS         :=
TARGET          := audioMixer
SRCDIR          := src
OBJDIR          := obj
INCPATH         := -Isrc/ 
LIBPATH         := 

SRC     := src/main.cpp                       \
           src/Receiver.cpp                   \
           src/ErrorHandler.cpp               \
           src/mixerModule/combineAudio.cpp   \
           src/mixerModule/combineUtils.cpp   

LIBS    := -lavcodec -lswscale -lswresample -lspeex -lavutil -lavformat -lx264

default: $(TARGET)

all: default
    
$(TARGET): $(SRC)
		@echo 'Building target: $(TARGET)'
		$(CC) $(CFLAGS) $(LDFLAGS) $(INCPATH) $(LIBPATH) -o $@ $^ $(LIBS)
		@echo 'Build finished'

.PHONY: clean default all

clean:
		-$(RM) -f src/*.o
