

#define ERROR_MESSAGE0(T,O,M) \
    do { \
        char 			str[1024]; \
        ErrorHandler*	pErrHandler = ErrorHandler::instance(); \
        snprintf(str, 1024, "%s", M); \
        pErrHandler->ErrorMessage(T, O, str); \
    } while (0)

#define ERROR_MESSAGE1(T,O,M,P1) \
    do { \
        char 			str[1024]; \
        ErrorHandler*	pErrHandler = ErrorHandler::instance(); \
        snprintf(str, 1024, M, P1); \
        pErrHandler->ErrorMessage(T, O, str); \
    } while (0)

#define ERROR_MESSAGE2(T,O,M,P1,P2) \
    do { \
        char 			str[1024]; \
        ErrorHandler*	pErrHandler = ErrorHandler::instance(); \
        snprintf(str, 1024, M, P1, P2); \
        pErrHandler->ErrorMessage(T, O, str); \
    } while (0)

#define ERROR_MESSAGE3(T,O,M,P1,P2,P3) \
    do { \
        char 			str[1024]; \
        ErrorHandler*	pErrHandler = ErrorHandler::instance(); \
        snprintf(str, 1024, M, P1, P2, P3); \
        pErrHandler->ErrorMessage(T, O, str); \
    } while (0)


#define ERROR_MESSAGE4(T,O,M,P1,P2,P3,P4) \
    do { \
        char 			str[1024]; \
        ErrorHandler*	pErrHandler = ErrorHandler::instance(); \
        snprintf(str, 1024, M, P1, P2, P3, P4); \
        pErrHandler->ErrorMessage(T, O, str); \
    } while (0)


#ifdef _DEBUG

#define DEBUG_MESSAGE0(O,M) \
    do { \
        char 			str[1024]; \
        ErrorHandler*	pErrHandler = ErrorHandler::instance(); \
        snprintf(str, 1024, "%s", M); \
        pErrHandler->DebugMessage(O, str); \
    } while (0)

#define DEBUG_MESSAGE1(O,M,P1) \
    do { \
        char 			str[1024]; \
        ErrorHandler*	pErrHandler = ErrorHandler::instance(); \
        snprintf(str, 1024, M, P1); \
        pErrHandler->DebugMessage(O, str); \
    } while (0)

#define DEBUG_MESSAGE2(O,M,P1,P2) \
    do { \
        char 			str[1024]; \
        ErrorHandler*	pErrHandler = ErrorHandler::instance(); \
        snprintf(str, 1024, M, P1, P2); \
        pErrHandler->DebugMessage(O, str); \
    } while (0)

#define DEBUG_MESSAGE3(O,M,P1,P2,P3) \
    do { \
        char 			str[1024]; \
        ErrorHandler*	pErrHandler = ErrorHandler::instance(); \
        snprintf(str, 1024, M, P1, P2, P3); \
        pErrHandler->DebugMessage(O, str); \
    } while (0)


#define DEBUG_MESSAGE4(O,M,P1,P2,P3,P4) \
    do { \
        char 			str[1024]; \
        ErrorHandler*	pErrHandler = ErrorHandler::instance(); \
        snprintf(str, 1024, M, P1, P2, P3, P4); \
        pErrHandler->DebugMessage(O, str); \
    } while (0)

#else
    #define DEBUG_MESSAGE0(O,M)
    #define DEBUG_MESSAGE1(O,M,P1)
    #define DEBUG_MESSAGE2(O,M,P1,P2)
    #define DEBUG_MESSAGE3(O,M,P1,P2,P3)
    #define DEBUG_MESSAGE4(O,M,P1,P2,P3,P4)
#endif




enum ErrorCode
{
    OK,
    ERROR
};

enum ErrorType
{
    ERR_TYPE_MESSAGE,       // Just log message
    ERR_TYPE_WARNING,       // Warning
    ERR_TYPE_DISPOSABLE,    // Kind of small error
    ERR_TYPE_ERROR,         // Usual error
    ERR_TYPE_CRITICAL,      // Critical error
    MAX_ERR_TYPES
};

/*
 * Class for handling all types of errors and debug log messages
 * It can not only log these messages to file, but also we can add some kind of
 * signalling here, such error notification to other apllications (SWB, Admin panel, etc)
 * For example signalling can be implemented via http protocol or sockets
 */
class ErrorHandler
{
public:
    static const char* DefaultLogFileName;

    static ErrorHandler* instance();

    ~ErrorHandler();

    /// Open file for dumping messages
    ErrorCode  SetLogFile(char* logFileName);

    /// Runs error handling (logging, signalling to other apps, etc)
    void ErrorMessage(ErrorType type, const char * ownerName, const char * message);

    /// Runs debug message handling (logging, signalling to other apps, etc)
    void DebugMessage(const char * ownerName, const char * message);

private:
    ErrorHandler();
    static ErrorHandler* m_instance;

    FILE*	m_hFile;
};
