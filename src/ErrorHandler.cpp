#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#include "ErrorHandler.h"

ErrorHandler* ErrorHandler::m_instance = NULL;

const char* ErrorHandler::DefaultLogFileName = "ErrorLog.txt";

const char* ErrorTypeString[MAX_ERR_TYPES] = {"MESSAGE", "WARNING", "DISPOSABLE", "ERROR", "CRITICAL"};


ErrorHandler* ErrorHandler::instance()
{
    if(!m_instance)
    {
        m_instance = new ErrorHandler();
    }
    return m_instance;
}

ErrorHandler::ErrorHandler()
{
    m_hFile = NULL;
    //m_hFile = fopen(DefaultLogFileName, "w");

    //if (NULL == m_hFile)
    //{
    //    ErrorMessage(ERR_TYPE_ERROR, "ErrorHandler", "Unable to open error log file for writing");
    //}
}

ErrorHandler::~ErrorHandler()
{
    fclose(m_hFile);
}

ErrorCode ErrorHandler::SetLogFile(char* pLogFileName)
{
    if (NULL != m_hFile)
    {
        fclose(m_hFile);
    }

    m_hFile = fopen(pLogFileName, "w");

    if (NULL == m_hFile)
    {
        ErrorMessage(ERR_TYPE_ERROR, "ErrorHandler", "Unable to open error log file for writing");
        return ERROR;
    }
    return OK;
}

void ErrorHandler::ErrorMessage(ErrorType errType, const char * ownerName, const char * message)
{
    char			errMsg[1024];
    struct timeval 	tv;
    time_t 			nowtime;
    struct tm* 		nowtm;
    char 			tmbuf[64];
    char			timeStr[64];

    gettimeofday(&tv, NULL);
    nowtime = tv.tv_sec;
    nowtm = localtime(&nowtime);
    strftime(tmbuf, sizeof(tmbuf), "%Y-%m-%d %H:%M:%S", nowtm);
    snprintf(timeStr, sizeof(timeStr), "%s.%03d", tmbuf, (int32_t)(tv.tv_usec / 1000));

    sprintf(errMsg, "%s\t[%s] %s: %s\n", timeStr, ErrorTypeString[errType], ownerName, message);

    printf("%s", errMsg);

    if(NULL != m_hFile)
    {
        fprintf(m_hFile, "%s", errMsg);
        fflush(m_hFile);
    }
}

void ErrorHandler::DebugMessage(const char * ownerName, const char * message)
{
    char			msg[1024];
    struct timeval 	tv;
    time_t 			nowtime;
    struct tm* 		nowtm;
    char 			tmbuf[64];
    char			timeStr[64];

    gettimeofday(&tv, NULL);
    nowtime = tv.tv_sec;
    nowtm = localtime(&nowtime);
    strftime(tmbuf, sizeof(tmbuf), "%Y-%m-%d %H:%M:%S", nowtm);
    snprintf(timeStr, sizeof(timeStr), "%s.%03d", tmbuf, (int32_t)(tv.tv_usec / 1000));

    sprintf(msg, "%s\t[DEBUG] %s: %s\n", timeStr, ownerName, message);

    printf("%s", msg);

    if(NULL != m_hFile)
    {
        fprintf(m_hFile, "%s", msg);
        fflush(m_hFile);
    }
}
