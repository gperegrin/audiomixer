/*
 * Copyright (C) Gennadiy Peregrin
 */

#include "Receiver.h"

AudioStreamCtx::AudioStreamCtx()
{
    for (int i=0; i < NUM_AUDIO_STREAMS; i++)
    {
        pSwrCtx[i] = NULL;
        pDecoder[i] = NULL;
        pDecoderCtx[i] = NULL;
        decoderCtxInitialized[i] = 0;
    }

    frameBuffer[STREAM_1].streamIndex = 1;
    frameBuffer[STREAM_2].streamIndex = 2;

    pEncoder = NULL;
    pEncoderCtx = NULL;
    pFrameToEncode = NULL;
    encodedFramePts = 0;
}

AudioStreamCtx::~AudioStreamCtx()
{
    for (int i=0; i < NUM_AUDIO_STREAMS; i++)
    {
        if (pDecoderCtx[i])
        {
            avcodec_close(pDecoderCtx[i]);
            avcodec_free_context(&pDecoderCtx[i]);
        }
        swr_free(&pSwrCtx[i]);
    }

    avcodec_close(pEncoderCtx);
    avcodec_free_context(&pEncoderCtx);
}

int32_t Receiver::initAudioDecoderContext(int streamIndex, AVCodecParameters* pCodecParams)
{
    DEBUG_MESSAGE1("Receiver", "initAudioDecoderContext() called for streamIndex = %d", streamIndex);

    // Allocate decoder context
    m_audio.pDecoderCtx[streamIndex] = avcodec_alloc_context3(m_audio.pDecoder[streamIndex]);
    if (NULL == m_audio.pDecoderCtx[streamIndex])
    {
        ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "initAudioDecoderContext() failed to allocate audio decoder context");
        return -1;
    }

    avcodec_parameters_to_context(m_audio.pDecoderCtx[streamIndex], pCodecParams);

    // Open decoder
    if (0 > avcodec_open2(m_audio.pDecoderCtx[streamIndex], m_audio.pDecoder[streamIndex], NULL))
    {
        ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "initAudioDecoderContext() failed to open audio decoder");
        return -1;
    }

    // Clear previously allocate SWR context
    if (NULL != m_audio.pSwrCtx[streamIndex])
    {
        swr_free(&m_audio.pSwrCtx[streamIndex]);
    }

    // Set up SWR context once we've got codec information
    if (m_audio.pDecoderCtx[streamIndex]->sample_rate != OUTPUT_AUDIO_SAMPLERATE)
    {
        // AAC default sample format is FLTP
        // SPEEX default sample format is S16P
        m_audio.pSwrCtx[streamIndex] = swr_alloc_set_opts(	NULL,
                                                            av_get_default_channel_layout(OUTPUT_AUDIO_CHANNELS),
                                                            AV_SAMPLE_FMT_FLTP,
                                                            OUTPUT_AUDIO_SAMPLERATE,
                                                            m_audio.pDecoderCtx[streamIndex]->channel_layout,
                                                            m_audio.pDecoderCtx[streamIndex]->sample_fmt,
                                                            m_audio.pDecoderCtx[streamIndex]->sample_rate,
                                                            0, NULL);

        if (NULL == m_audio.pSwrCtx[streamIndex])
        {
            ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "initAudioDecoderContext() failed to allocate swr context");
            return -1;
        }

        if (0 > swr_init(m_audio.pSwrCtx[streamIndex]))
        {
            ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "initAudioDecoderContext() failed to init swr context");
            return -1;
        }
    }
    else
    {
        m_audio.pSwrCtx[streamIndex] = NULL;	// We don't need the resampling
    }

    // Mark decoder context as initialized
    m_audio.decoderCtxInitialized[streamIndex] = 1;

    return 0;
}

int32_t Receiver::addAudioFrame(AVPacket* pPacket, int streamIndex)
{
    // If we have initialized decoder context
    if (m_audio.decoderCtxInitialized[streamIndex])
    {
        AVFrame*	pFrame = av_frame_alloc();

        // Decode, resample and add frames while they are present in packet
        int32_t sendRes = avcodec_send_packet(m_audio.pDecoderCtx[streamIndex], pPacket);
        int32_t decodeRes = avcodec_receive_frame(m_audio.pDecoderCtx[streamIndex], pFrame);

        if (sendRes != 0 || decodeRes != 0)
        {
            char str[256] = {0};
            av_make_error_string(str, 255, sendRes);
            ERROR_MESSAGE1(ERR_TYPE_ERROR, "Receiver", "addAudioFrame() avcodec_send_packet() returned %s", str);
            av_make_error_string(str, 255, decodeRes);
            ERROR_MESSAGE1(ERR_TYPE_ERROR, "Receiver", "addAudioFrame() avcodec_receive_frame() returned %s", str);
            return -1;
        }

        while (0 == decodeRes)
        {
            if (NULL != m_audio.pSwrCtx[streamIndex])
            {
                const int   outSamplesNum = 8192; // Must be enough for rsample 1024-samples frame from 8000 to 48000
                uint8_t**   ppOut = NULL;
                int         outLinesize;
                int         numSamples;

                if (0 > av_samples_alloc_array_and_samples(&ppOut, &outLinesize, 2, outSamplesNum, AV_SAMPLE_FMT_FLTP, 32))
                {
                    ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "addAudioFrame() failed to allocate array for resampled data");
                    return -1;
                }

                numSamples = swr_convert(m_audio.pSwrCtx[streamIndex], ppOut, outSamplesNum, (const uint8_t**)pFrame->data, pFrame->nb_samples);

                // This part should be locked to avoid simultaneous access from different threads
                m_audio.mutex.lock();
                m_audio.frameBuffer[streamIndex].addFrameFLT(ppOut, 2, numSamples);
                m_audio.mutex.unlock();

                // Free out samples memory
                if (ppOut[0])
                {
                    av_freep(&ppOut[0]);
                }
                av_freep(&ppOut);
            }
            else
            {
                if (pFrame->format == AV_SAMPLE_FMT_FLTP || (pFrame->format == AV_SAMPLE_FMT_FLT && pFrame->channels == 1))
                {
                    // This part should be locked to avoid simultaneous access from different threads
                    m_audio.mutex.lock();
                    m_audio.frameBuffer[streamIndex].addFrameFLT(pFrame->data, pFrame->channels, pFrame->nb_samples);
                    m_audio.mutex.unlock();
                }
                else if (pFrame->format == AV_SAMPLE_FMT_S16P || (pFrame->format == AV_SAMPLE_FMT_S16 && pFrame->channels == 1))
                {
                    // This part should be locked to avoid simultaneous access from different threads
                    m_audio.mutex.lock();
                    m_audio.frameBuffer[streamIndex].addFrameS16(pFrame->data, pFrame->channels, pFrame->nb_samples);
                    m_audio.mutex.unlock();
                }
            }
            // Try to decode other frames in packet
            decodeRes = avcodec_receive_frame(m_audio.pDecoderCtx[streamIndex], pFrame);
        }
        av_frame_free(&pFrame);
    }
    else
    {
        ERROR_MESSAGE1(ERR_TYPE_WARNING, "Receiver", "Stream #%d addAudioFrame() called but audio decoder context was not initialized", streamIndex);
    }
    return 0;
}

static float mixAudioSamples(float a, float b, float la, float lb)
{
    return (a*la + b*lb);
}

int32_t Receiver::getEncodeAndWriteAudioFrame()
{
    int         i;
    int 		sendRes = 0;
    int         encodeRes = 0;
    int         writeRes = 0;
    AVPacket    packet;

    float       pFrame1[2048];
    float       pFrame2[2048];
    float*      pChannel1 = (float *)m_audio.pFrameToEncode->data[0];
    float*      pChannel2 = (float *)m_audio.pFrameToEncode->data[1];

    memset(pFrame1, 0, 2048*sizeof(float));
    memset(pFrame2, 0, 2048*sizeof(float));

    // This part should be locked to avoid simultaneous access from different threads
    m_audio.frameBuffer[STREAM_1].getFrame(pFrame1, 2048, m_audio.mutex);
    m_audio.frameBuffer[STREAM_2].getFrame(pFrame2, 2048, m_audio.mutex);

    for (i = 0; i < 1024; i++)
    {
        pChannel2[i] = mixAudioSamples(pFrame1[i*2], pFrame2[i*2 + 1], m_audio.levels[STREAM_1], m_audio.levels[STREAM_2]);
        pChannel1[i] = mixAudioSamples(pFrame2[i*2], pFrame1[i*2 + 1], m_audio.levels[STREAM_2], m_audio.levels[STREAM_1]);
    }

    m_audio.pFrameToEncode->nb_samples = 1024;

    // Got frame - encode and write it

    packet.size = 0;
    packet.data = NULL;
    av_init_packet(&packet);

    m_audio.pFrameToEncode->pts = m_audio.encodedFramePts;

    sendRes = avcodec_send_frame(m_audio.pEncoderCtx, m_audio.pFrameToEncode);
    encodeRes = avcodec_receive_packet(m_audio.pEncoderCtx, &packet);

    if (encodeRes == AVERROR(EAGAIN))
    {
        return 0;
    }

    if (0 != encodeRes || 0 != sendRes)
    {
        char str[256] = {0};
        av_make_error_string(str, 255, sendRes);
        ERROR_MESSAGE1(ERR_TYPE_ERROR, "Receiver", "encodeAndWriteAudioFrame() avcodec_send_frame() returned %s", str);
        av_make_error_string(str, 255, encodeRes);
        ERROR_MESSAGE1(ERR_TYPE_ERROR, "Receiver", "encodeAndWriteAudioFrame() avcodec_receive_packet() returned %s", str);
        return -1;
    }

    packet.stream_index = 1;	// Audio stream
    av_packet_rescale_ts(&packet, m_audio.pEncoderCtx->time_base, m_pFormatCtx->streams[1]->time_base);
    writeRes = av_interleaved_write_frame(m_pFormatCtx, &packet);

    if (0 != writeRes)
    {
        char str[256] = {0};
        av_make_error_string(str, 255, writeRes);
        ERROR_MESSAGE1(ERR_TYPE_ERROR, "Receiver", "encodeAndWriteAudioFrame() av_interleaved_write_frame() returned %s", str);
        return -1;
    }

    m_audio.encodedFramePts += m_audio.pFrameToEncode->nb_samples;
    m_outAudioMicros += av_rescale_q(m_audio.pFrameToEncode->nb_samples, m_audio.pEncoderCtx->time_base, AV_TIME_BASE_Q);
    return 1;
}
