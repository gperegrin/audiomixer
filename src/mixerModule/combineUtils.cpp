/*
 * Copyright (C) Gennadiy Peregrin
 */

#include <unistd.h>

#include "Receiver.h"

/////////////////////////////////////////////
/////////// Frame buffer utils //////////////
/////////////////////////////////////////////

AudioCircularBuffer::AudioCircularBuffer() :
    m_totalRead(0),
    m_totalWrite(0),
    m_currentReadIndex(0),
    m_currentWriteIndex(0)
{
    m_pBuffer = new float[AUDIO_FRAME_BUFFER_SIZE];
    memset(m_pBuffer, 0, AUDIO_FRAME_BUFFER_SIZE * sizeof(float));
}

AudioCircularBuffer::~AudioCircularBuffer()
{
    SAFE_DELETE_ARRAY(m_pBuffer);
}

int32_t	AudioCircularBuffer::getFrame(float *pBuf, int32_t numSamples, std::mutex& mu)
{
    int  attempts = 0;

    while (attempts < 10)
    {
        if ((m_totalWrite - m_totalRead) > numSamples)
        {
            mu.lock();
            // Returns raw audio data (float samples!). Channels are interleaved!!!
            for (int i = 0; i < numSamples; i++)
            {
                pBuf[i] = m_pBuffer[m_currentReadIndex];

                // Update index and counters
                m_totalRead++;
                m_currentReadIndex = (m_currentReadIndex + 1) & (AUDIO_FRAME_BUFFER_SIZE - 1);
            }
            mu.unlock();

            DEBUG_MESSAGE3("AudioCircularBuffer", "Stream #%d: Audio frame read. Attempts = %d, Samples count = %ld", streamIndex, attempts, m_totalWrite - m_totalRead);
            return 1;
        }
        else
        {
            usleep(1000);
            attempts++;
        }
    }
    DEBUG_MESSAGE1("AudioCircularBuffer", "Stream #%d: Could not read audio frame after 10 attempts", streamIndex);
    return 0;
}

void AudioCircularBuffer::resetBuffer()
{
    m_totalRead = 0;
    m_totalWrite = 0;
    m_currentReadIndex = 0;
    m_currentWriteIndex = 0;
}

int32_t	AudioCircularBuffer::addFrameFLT(uint8_t **ppBuf, int32_t channels, int32_t numSamples)
{
    float*  pIn1;
    float*  pIn2;
    int32_t samplesInBuf = m_totalWrite - m_totalRead;

    pIn1 = (float *)ppBuf[0];
    pIn2 = (channels > 1) ? (float *)ppBuf[1] : NULL;

    if ((samplesInBuf + numSamples*channels) > AUDIO_FRAME_BUFFER_SIZE)
    {
        ERROR_MESSAGE2(ERR_TYPE_WARNING, "AudioCircularBuffer", "Stream #%d: Audio buffer overflow. Skipping %d samples", streamIndex, samplesInBuf);
        resetBuffer();
    }

    for (int i = 0; i < numSamples; i++)
    {
        // Put sample from channel 1
        m_pBuffer[m_currentWriteIndex] = pIn1[i];

        // Update index and counters
        m_totalWrite++;
        m_currentWriteIndex = (m_currentWriteIndex + 1) & (AUDIO_FRAME_BUFFER_SIZE - 1);

        // Put sample from channel 1
        m_pBuffer[m_currentWriteIndex] = (pIn2 != NULL) ? pIn2[i] : pIn1[i];

        // Update index and counters
        m_totalWrite++;
        m_currentWriteIndex = (m_currentWriteIndex + 1) & (AUDIO_FRAME_BUFFER_SIZE - 1);
    }
    return 0;
}

int32_t	AudioCircularBuffer::addFrameS16(uint8_t **ppBuf, int32_t channels, int32_t numSamples)
{
    int16_t* pIn1;
    int16_t* pIn2;
    int32_t samplesInBuf = m_totalWrite - m_totalRead;

    pIn1 = (int16_t *)ppBuf[0];
    pIn2 = (channels > 1) ? (int16_t *)ppBuf[1] : NULL;

    if ((samplesInBuf + numSamples*channels) > AUDIO_FRAME_BUFFER_SIZE)
    {
        ERROR_MESSAGE2(ERR_TYPE_WARNING, "AudioCircularBuffer", "Stream #%d: Audio buffer overflow. Skipping %d samples", streamIndex, samplesInBuf);
        resetBuffer();
    }

    for (int i = 0; i < numSamples; i++)
    {
        // Put sample from channel 1
        m_pBuffer[m_currentWriteIndex] = ((float)pIn1[i] / 32768.0f);

        // Update index and counters
        m_totalWrite++;
        m_currentWriteIndex = (m_currentWriteIndex + 1) & (AUDIO_FRAME_BUFFER_SIZE - 1);

        // Put sample from channel 1
        m_pBuffer[m_currentWriteIndex] = (pIn2 != NULL) ? ((float)pIn2[i] / 32768.0f) : ((float)pIn1[i] / 32768.0f);

        // Update index and counters
        m_totalWrite++;
        m_currentWriteIndex = (m_currentWriteIndex + 1) & (AUDIO_FRAME_BUFFER_SIZE - 1);
    }
    return 0;
}
