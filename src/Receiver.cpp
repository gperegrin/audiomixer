/*
 * Copyright (C) Gennadiy Peregrin
 */
#include <signal.h>
#include "unistd.h"
#include "sys/time.h"

#include "Receiver.h"

// Sigterm handler will raise this flag for stopping
extern sig_atomic_t TerminateFlag;

Receiver::Receiver(
    const char* url,
    const char* fullName,
    const char* inApp,
    const char* transApp,
    const char* outApp,
    float l1,
    float l2,
    float mainBufLen)
{
    AVStream*       pVideoStream;
    AVStream*       pAudioStream;

    char            name[128] = {0};
    char            suffix[32] = {0};

    m_videoStreamIdx[STREAM_1] = -1;
    m_audioStreamIdx[STREAM_1] = -1;
    m_videoStreamIdx[STREAM_2] = -1;
    m_audioStreamIdx[STREAM_2] = -1;

    m_pInputContext[STREAM_1] = NULL;
    m_pInputContext[STREAM_2] = NULL;
    m_pFormatCtx = NULL;

    m_outputDTS = 0;
    m_outVideoMicros = 0;
    m_outAudioMicros = 0;
    m_firstVideoTS[STREAM_1] = AV_NOPTS_VALUE;
    m_firstVideoTS[STREAM_2] = AV_NOPTS_VALUE;

    m_initialized = false;

    m_audio.levels[STREAM_1] = l1;
    m_audio.levels[STREAM_2] = l2;

    // Separate main name and suffix
    const char* sep = strstr(fullName, "_");

    if (NULL != sep)
    {
        memcpy(name, fullName, (int)(sep - fullName));
        strncpy(suffix, sep, 31);
    }
    else
    {
        strncpy(name, fullName, 127);
    }

    memset(m_urls, 0, sizeof(m_urls));

    // Input url like rtmp://localhost/main/test_hi
    strncpy(m_urls[0], url, 350);
    strcat(m_urls[0], "/");
    strcat(m_urls[0], inApp);
    strcat(m_urls[0], "/");
    strcat(m_urls[0], name);
    strcat(m_urls[0], suffix);

    // Input translator's url like rtmp://localhost/lang_rus/test
    strncpy(m_urls[1], url, 350);
    strcat(m_urls[1], "/");
    strcat(m_urls[1], transApp);
    strcat(m_urls[1], "/");
    strcat(m_urls[1], name);

    // Input translator's url like rtmp://localhost/lang_rus/test
    strncpy(m_urls[2], url, 350);
    strcat(m_urls[2], "/");
    strcat(m_urls[2], outApp);
    strcat(m_urls[2], "/");
    strcat(m_urls[2], name);
    strcat(m_urls[2], "_");
    strcat(m_urls[2], transApp);
    strcat(m_urls[2], suffix);

    // Stream 1
    openInput(STREAM_1);

    // Get number of delayed frames for master stream (requrement from customer)
    m_delayedFramesNumber = (int)(mainBufLen * av_q2d(m_pInputContext[STREAM_1]->streams[m_videoStreamIdx[STREAM_1]]->avg_frame_rate) + 0.5f);

    if (m_videoStreamIdx[STREAM_1] < 0)
    {
        ERROR_MESSAGE0(ERR_TYPE_CRITICAL, "Receiver", "Primary video stream not found. Exiting");
        return;
    }

    if (m_audioStreamIdx[STREAM_1] < 0)
    {
        ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "Primary audio stream not found. Exiting");
        return;
    }

    /////////////////////////
    // Open output context //
    /////////////////////////
    ERROR_MESSAGE1(ERR_TYPE_MESSAGE, "Receiver", "Opening output at %s...", m_urls[2]);

    // Open output format context
    avformat_alloc_output_context2(&m_pFormatCtx, NULL, "flv", m_urls[2]);
    if (NULL == m_pFormatCtx)
    {
        ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "Failed to allocate output format context");
        return;
    }

    // Creating output video stream
    pVideoStream = avformat_new_stream(m_pFormatCtx, NULL); //m_pInputContext[STREAM_1]->streams[m_videoStreamIdx[STREAM_1]]->codec);
    if (NULL == pVideoStream)
    {
        ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "Failed to create output video stream");
        return;
    }

    avcodec_parameters_copy(pVideoStream->codecpar, m_pInputContext[STREAM_1]->streams[m_videoStreamIdx[STREAM_1]]->codecpar);

    // Default video stream index
    pVideoStream->index = 0;

    pVideoStream->time_base = m_pInputContext[STREAM_1]->streams[m_videoStreamIdx[STREAM_1]]->time_base;
    pVideoStream->avg_frame_rate = m_pInputContext[STREAM_1]->streams[m_videoStreamIdx[STREAM_1]]->avg_frame_rate;
    pVideoStream->start_time = 0;
    pVideoStream->codecpar->codec_tag = 0;
    // Creating output audio stream

    // Find audio encoder
    m_audio.pEncoder = avcodec_find_encoder(AV_CODEC_ID_AAC);
    if (NULL == m_audio.pEncoder)
    {
        ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "Failed to find aac encoder");
        return;
    }

    pAudioStream = avformat_new_stream(m_pFormatCtx, m_audio.pEncoder);
    if (NULL == pAudioStream)
    {
        ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "Failed to create output audio stream");
        return;
    }
    pAudioStream->time_base = av_make_q(1, OUTPUT_AUDIO_SAMPLERATE);

    // Default audio stream index
    pAudioStream->index = 1;

    // Set up audio encoder context
    m_audio.pEncoderCtx = avcodec_alloc_context3(m_audio.pEncoder);
    if (NULL == m_audio.pEncoderCtx)
    {
        ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "Output audio codec is null");
        return;
    }

    avcodec_get_context_defaults3(m_audio.pEncoderCtx, m_audio.pEncoder);

    m_audio.pEncoderCtx->codec_type = AVMEDIA_TYPE_AUDIO;
    m_audio.pEncoderCtx->sample_fmt = AV_SAMPLE_FMT_FLTP;
    m_audio.pEncoderCtx->bit_rate = OUTPUT_AUDIO_BITRATE;
    m_audio.pEncoderCtx->sample_rate = OUTPUT_AUDIO_SAMPLERATE;
    m_audio.pEncoderCtx->channels = OUTPUT_AUDIO_CHANNELS;
    m_audio.pEncoderCtx->channel_layout = AV_CH_LAYOUT_STEREO;
    m_audio.pEncoderCtx->profile = FF_PROFILE_AAC_LOW;
    m_audio.pEncoderCtx->time_base = av_make_q(1, m_audio.pEncoderCtx->sample_rate);
    // This is required to allow using default ffmpeg's AAC encoder which is experimental (and now ideas why)
    m_audio.pEncoderCtx->strict_std_compliance = FF_COMPLIANCE_EXPERIMENTAL;

    if (m_pFormatCtx->oformat->flags & AVFMT_GLOBALHEADER)
        m_audio.pEncoderCtx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

    // Open audio encoder
    if (0 > avcodec_open2(m_audio.pEncoderCtx, m_audio.pEncoder, NULL))
    {
        ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "Failed to open aac encoder");
        return;
    }

    // Copy audio codec parameters to audio stream
    avcodec_parameters_from_context(pAudioStream->codecpar, m_audio.pEncoderCtx);

    pAudioStream->codecpar->codec_tag = 0;

    // Open output file, if it is allowed by format
    if (0 > avio_open(&m_pFormatCtx->pb, m_urls[2], AVIO_FLAG_WRITE))
    {
        ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "Failed to open output avio device");
        return;
    }

    {
        AVDictionary*   options(0);
        av_dict_set(&options, "aac_seq_header_detect", "1", 0);
        // Write format header to output stream
        if (0 > avformat_write_header(m_pFormatCtx, &options))
        {
            ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "Cannot write output format header");
            return;
        }
        av_dict_free(&options);
    }

    av_dump_format(m_pFormatCtx, 0, m_urls[2], 1);

    // Allocate and init audio frame to be encoded
    m_audio.pFrameToEncode = av_frame_alloc();
    if (NULL == m_audio.pFrameToEncode)
    {
        ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "Failed to allocate audio frame object");
        return;
    }

    m_audio.pFrameToEncode->format = AV_SAMPLE_FMT_FLTP;
    m_audio.pFrameToEncode->channels = OUTPUT_AUDIO_CHANNELS;
    m_audio.pFrameToEncode->nb_samples = m_audio.pEncoderCtx->frame_size;
    m_audio.pFrameToEncode->channel_layout = AV_CH_LAYOUT_STEREO;

    if (0 > av_frame_get_buffer(m_audio.pFrameToEncode, 32))  // allocates aligned buffer for audio data
    {
        ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "Failed to allocate audio frame buffers");
        return;
    }

    m_initialized = true;
    m_terminated = false;
}

Receiver::~Receiver()
{
    ERROR_MESSAGE0(ERR_TYPE_MESSAGE, "Receiver", "Receiver destructor called");
    if (NULL != m_pInputContext[STREAM_1])
    {
        avformat_close_input(&m_pInputContext[STREAM_1]);
    }

    if (NULL != m_pInputContext[STREAM_2])
    {
        avformat_close_input(&m_pInputContext[STREAM_2]);
    }

    if (NULL != m_pFormatCtx)
    {
        avformat_free_context(m_pFormatCtx);
    }
}

static int64_t GetCurMSec()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (tv.tv_sec*1000) + (tv.tv_usec / 1000);
}

int32_t Receiver::openInput(int streamIndex)
{
    ERROR_MESSAGE2(ERR_TYPE_MESSAGE, "Receiver", "Opening input context #%d at %s...", streamIndex + 1, m_urls[streamIndex]);

    if (NULL != m_pInputContext[streamIndex])
    {
        ERROR_MESSAGE1(ERR_TYPE_MESSAGE, "Receiver", "Closing previous input context for stream #%d", streamIndex + 1);
        avformat_close_input(&m_pInputContext[streamIndex]);
    }

    // Close audio decoder context, if needed
    if (NULL != m_audio.pDecoderCtx[streamIndex])
    {
        ERROR_MESSAGE1(ERR_TYPE_MESSAGE, "Receiver", "Closing previous audio decoder context for stream #%d", streamIndex + 1);
        avcodec_free_context(&m_audio.pDecoderCtx[streamIndex]);
    }

    if (0 > avformat_open_input(&m_pInputContext[streamIndex], m_urls[streamIndex], NULL, NULL))
    {
        ERROR_MESSAGE1(ERR_TYPE_CRITICAL, "Receiver", "Failed to open input stream #%d", streamIndex + 1);
        return -1;
    }

    if (0 > avformat_find_stream_info(m_pInputContext[streamIndex], NULL))
    {
        ERROR_MESSAGE1(ERR_TYPE_CRITICAL, "Receiver", "Failed to get stream info from stream #%d", streamIndex);
        return -1;
    }

    m_videoStreamIdx[streamIndex] = av_find_best_stream(m_pInputContext[streamIndex], AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
    m_audioStreamIdx[streamIndex] = av_find_best_stream(m_pInputContext[streamIndex], AVMEDIA_TYPE_AUDIO, -1, -1, &m_audio.pDecoder[streamIndex], 0);

    if (NULL == m_audio.pDecoder[streamIndex])
    {
        ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "Failed to find audio decoder");
        return -1;	// Audio codec not found
    }

    initAudioDecoderContext(streamIndex, m_pInputContext[streamIndex]->streams[m_audioStreamIdx[streamIndex]]->codecpar);

    ERROR_MESSAGE1(ERR_TYPE_MESSAGE, "Receiver", "Information about stream #%d:", streamIndex);

    // Dump information about file onto standard error
    av_dump_format(m_pInputContext[streamIndex], m_videoStreamIdx[streamIndex], m_urls[streamIndex], 0);
    av_dump_format(m_pInputContext[streamIndex], m_audioStreamIdx[streamIndex], m_urls[streamIndex], 0);

    return 0;
}

int32_t Receiver::writeVideoFrame(AVPacket *pPacket)
{
    AVRational  srcTimebase = m_pInputContext[STREAM_1]->streams[m_videoStreamIdx[STREAM_1]]->time_base;
    int         ptsDelta = (double)(pPacket->pts - pPacket->dts) / (double)(pPacket->duration + 1) + 0.3333333333;
    int64_t     outDuration = av_rescale_q(pPacket->duration, srcTimebase, m_pFormatCtx->streams[0]->time_base);

    pPacket->stream_index = 0; // Default video stream index
    pPacket->dts = m_outputDTS;
    pPacket->pts = m_outputDTS + ptsDelta*outDuration;
    pPacket->duration = outDuration;

    m_outputDTS += outDuration;
    m_outVideoMicros += av_rescale_q(outDuration, m_pFormatCtx->streams[0]->time_base, AV_TIME_BASE_Q);

    int res = av_interleaved_write_frame(m_pFormatCtx, pPacket);
    if (res < 0)
    {
        char str[255];
        av_strerror(res, str, 255);
        ERROR_MESSAGE1(ERR_TYPE_ERROR, "Receiver::Master", "encodeAndWriteVideoFrame() failed, error: %s", str);
        return -1;
    }
    return 0;
}

void Receiver::readingThread1()
{
    int         readRes = 0;
    int         numReadErrors = 0;
    AVPacket    packet;
    int64_t		encodingTime;
    int64_t		sleepTime = 2;  // 2 ms sleep between reading frames should be ok for both threads to read

    // Inititalize packet
    av_init_packet(&packet);

    // Start reading and send frames to combine
    while ((!m_terminated) && (readRes >= 0))
    {
        readRes = av_read_frame(m_pInputContext[STREAM_1], &packet);

        if (readRes != 0)
        {
            if (++numReadErrors < 300)
            {
                continue;
            }
            else
            {
                ERROR_MESSAGE0(ERR_TYPE_CRITICAL, "Receiver::Master", "300 read frame error in a row. Exiting");
                break;
            }
        }
        numReadErrors = 0;

        if (packet.stream_index == m_videoStreamIdx[STREAM_1])
        {
            int numAudioFrames = 0;
            encodingTime = GetCurMSec(); // Start time measurement

            DEBUG_MESSAGE2("Receiver::Master", "Read video packet. DTS = %ld, flags = %d", packet.dts, packet.flags);

            m_firstVideoTS[STREAM_1] = (m_firstVideoTS[STREAM_1] == AV_NOPTS_VALUE) ? packet.dts : m_firstVideoTS[STREAM_1];

            m_videoQueue.push(av_packet_clone(&packet));

            if (m_videoQueue.size() > m_delayedFramesNumber)
            {
                writeVideoFrame(m_videoQueue.front());
                m_videoQueue.pop();
            }

            while (m_outAudioMicros <= m_outVideoMicros)
            {
                int64_t  encodeAudioTime = GetCurMSec();
                getEncodeAndWriteAudioFrame();
                encodeAudioTime = GetCurMSec() - encodeAudioTime;
                numAudioFrames++;
                //DEBUG_MESSAGE1("Receiver::Master", "Audio frame written (%ld ms)", encodeAudioTime);
            }

            // Measure total time to get both audio frames (including waiting for secondary stream audio)
            encodingTime = GetCurMSec() - encodingTime;

            DEBUG_MESSAGE2("Receiver::Master", "Video and %d audio packet written (%ld ms)", numAudioFrames, encodingTime);
        }
        else if (packet.stream_index == m_audioStreamIdx[STREAM_1])
        {
            DEBUG_MESSAGE1("Receiver::Master", "Read audio packet. DTS = %ld", packet.dts);

            if (packet.dts < m_firstVideoTS[STREAM_1] || m_firstVideoTS[STREAM_1] == AV_NOPTS_VALUE)
            {
                continue;
            }

            if (0 != addAudioFrame(&packet, STREAM_1))
            {
                ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver::Master", "Error decoding audio frame");
            }
        }

        // To let secondary thread work
        //usleep(FFMAX(1, sleepTime - encodingTime)*1000);
        usleep(sleepTime*1000);
    }
    ERROR_MESSAGE0(ERR_TYPE_WARNING, "Receiver::Master", "Exiting thread...");
}

void Receiver::readingThread2()
{
    AVPacket    packet;
    int         isOpened;
    int64_t     totalAudioFrames = 0;
    int64_t		sleepTime = 5;  // 5 ms sleep between reading frames should be ok for both threads to read

    // Initially open input context for secondary stream
    do
    {
        isOpened = openInput(STREAM_2);
        usleep(1000);
    } while (isOpened < 0);

    // Inititalize packet
    av_init_packet(&packet);

    // Start reading and send frames to combine
    while (!m_terminated)
    {
        if (0 > av_read_frame(m_pInputContext[STREAM_2], &packet))
        {
            // Try to reopen input
            do
            {
                isOpened = openInput(STREAM_2);
                totalAudioFrames = 0;
                usleep(10000);
            } while (isOpened < 0);

            continue;
        }

        if (packet.stream_index == m_videoStreamIdx[STREAM_2])
        {
            DEBUG_MESSAGE2("Receiver::readingThread2", "Read video packet. DTS = %ld, flags = %d", packet.dts, packet.flags);
            m_firstVideoTS[STREAM_2] = (m_firstVideoTS[STREAM_2] == AV_NOPTS_VALUE) ? packet.dts : m_firstVideoTS[STREAM_2];
            // Free packet memory (allocated by av_read_frame)
            av_packet_unref(&packet);
        }
        else if (packet.stream_index == m_audioStreamIdx[STREAM_2])
        {
            DEBUG_MESSAGE1("Receiver::readingThread2", "Read audio packet. DTS = %ld", packet.dts);
            totalAudioFrames ++;

            if (totalAudioFrames > SKIP_INITIAL_AUDIO)
            {
                addAudioFrame(&packet, STREAM_2);
            }

            // Free packet memory (allocated by av_read_frame)
            av_packet_unref(&packet);
        }
        // Wait to let master thread work
        usleep(sleepTime*1000);
    }
    ERROR_MESSAGE0(ERR_TYPE_WARNING, "Receiver::readingThread2", "Exiting thread...");
}

void Receiver::startReadFrames()
{
    if (!m_initialized)
    {
        ERROR_MESSAGE0(ERR_TYPE_ERROR, "Receiver", "Input streams were not opened correctly. Exiting...");
        return;
    }

    m_outputDTS = 0;
    m_terminated = 0;
    TerminateFlag = 0;

    m_readThread1 = std::thread(&Receiver::readingThread1, this);
    m_readThread1.detach();

    m_readThread2 = std::thread(&Receiver::readingThread2, this);
    m_readThread2.detach();

    while(1)
    {
        m_terminated = TerminateFlag;
        usleep(100000);
    }
    ERROR_MESSAGE0(ERR_TYPE_WARNING, "Receiver", "Reach the end of function. Exiting...");
}


