
#ifndef RECEIVER_H_
#define RECEIVER_H_

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <thread>
#include <queue>
#include <mutex>

extern "C"
{
#include <libavcodec/avcodec.h>
#include <libswscale/swscale.h>
#include <libavformat/avformat.h>
#include <libswresample/swresample.h>
}

#include "ErrorHandler.h"

#define     NUM_VIDEO_STREAMS            2
#define     NUM_AUDIO_STREAMS            2

#define     STREAM_1                     0
#define     STREAM_2                     1

#define     COMBINE_MSG_VIDEO            9
#define     COMBINE_MSG_AUDIO            10

#define     WAIT_STREAM_INTERVAL         30000          // Interval for check streams disapeearing (ms)

#define     GET_AUDIO_ATTEMPTS           10             // How many ms to wait for new audio frame in queue
#define     SKIP_INITIAL_AUDIO           50             // How many audio frames should we skip at the beginning
#define     VIDEO_FRAME_BUFFER_SIZE      (512)          // Maximum number of video samples in queue (must be power of two)
#define     AUDIO_FRAME_BUFFER_SIZE      (512*2048)     // Maximum number of audio samples in queue (must be power of two)

#define     OUTPUT_AUDIO_BITRATE         128000         // Default bitrate for output audio stream
#define     OUTPUT_AUDIO_SAMPLERATE      44100          // Default sample rate for output audio stream
#define     OUTPUT_AUDIO_CHANNELS        2              // Default channels number for output audio stream
#define     RTMP_DEFAULT_TIMEBASE        1000           // Default timebase for rtmp streams (flv format)

// Helper macros and functions
#define SAFE_DELETE_ARRAY(arr)  if(NULL!=arr) { delete [] arr; arr = NULL; };
#define SAFE_DELETE(ptr)  if(NULL!=ptr) { delete ptr; ptr = NULL; };

class AudioCircularBuffer
{
public:
    AudioCircularBuffer();
    ~AudioCircularBuffer();

    void		   resetBuffer();
    int32_t        getFrame(float *pBuf, int32_t numSamples, std::mutex &mu);
    int32_t        addFrameFLT(uint8_t **ppBuf, int32_t channels, int32_t size);
    int32_t        addFrameS16(uint8_t **ppBuf, int32_t channels, int32_t size);

    int32_t        streamIndex;

private:
    float*         m_pBuffer;

    int64_t        m_totalRead;
    int64_t        m_totalWrite;
    int32_t        m_currentReadIndex;
    int32_t        m_currentWriteIndex;
};


class AudioStreamCtx
{
public:
    AudioStreamCtx();
    ~AudioStreamCtx();

    // Synchronization for add/get audio samples
    std::mutex                      mutex;

    // AVLib contexts for decoder
    AVCodec*                        pDecoder[NUM_AUDIO_STREAMS];
    AVCodecContext*                 pDecoderCtx[NUM_AUDIO_STREAMS];
    int32_t                         decoderCtxInitialized[NUM_AUDIO_STREAMS];
    AudioCircularBuffer             frameBuffer[NUM_AUDIO_STREAMS];

    float                           levels[NUM_AUDIO_STREAMS];

    // Resampler context (audio only)
    SwrContext*                     pSwrCtx[NUM_AUDIO_STREAMS];

    // AVLib contexts for encoder
    AVCodec*                        pEncoder;
    AVCodecContext*                 pEncoderCtx;
    AVFrame*                        pFrameToEncode;
    int64_t                         encodedFramePts;
};

class Receiver
{
public:
    Receiver(const char* url,
             const char* fullName,
             const char* inApp,
             const char* transApp,
             const char* outApp,
             float       l1,
             float       l2,
             float       mainBufLen);

    ~Receiver();

    void startReadFrames();
    void readingThread1();
    void readingThread2();

private:
    bool                    m_initialized;
    bool                    m_terminated;

    char                    m_urls[3][512];

    std::thread             m_readThread1;
    std::thread             m_readThread2;

    std::queue<AVPacket*>   m_videoQueue;
    unsigned int            m_delayedFramesNumber;
    AudioStreamCtx          m_audio;

    // Input timestamps
    int64_t                 m_firstVideoTS[2];

    // Output timestamp
    int64_t                 m_outputDTS;

    int64_t                 m_outAudioMicros;
    int64_t                 m_outVideoMicros;

    // AVLib format contexts for output stream
    AVFormatContext*        m_pFormatCtx;

    // AVLib format contexts for input streams
    AVFormatContext*        m_pInputContext[2];
    int                     m_videoStreamIdx[2];
    int                     m_audioStreamIdx[2];

    // Private functions
    int32_t  openInput(int streamIndex);
    int32_t  getCombinedAudioFrame();
    int32_t  writeVideoFrame(AVPacket* pPacket);
    int32_t  getEncodeAndWriteAudioFrame();
    int32_t  addAudioFrame(AVPacket* pPacket, int streamIndex);
    int32_t  initAudioDecoderContext(int32_t streamIndex, AVCodecParameters* pCodecParams);
    int32_t  closeInputAndDecoderContext(int32_t streamIndex);
};

#endif /* RECEIVER_H_ */
