#include <stdio.h>
#include <signal.h>
#include <iostream>

#include "Receiver.h"


// Sigterm handler will raise this flag for stopping
volatile sig_atomic_t TerminateFlag = 0;

void term(int )
{
    ERROR_MESSAGE0(ERR_TYPE_WARNING, "Global", "SIGTERM handled");
    TerminateFlag = 1;
}

int main(int argc, char **argv)
{
    struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = term;
    sigaction(SIGTERM, &action, NULL);

    if (argc < 8)
    {
        printf("USAGE: audioMixer <baseUrl> <full name> <input app> <translator's app> <output app> <level1> <level2> [buffering time] [log file name]\n" \
               "example ./audioMixer rtmp://localhost:1935 test_hi main lang1 output 1.0 0.7 0.3\n"
               "rtmp://localhost/main/test_hi\nrtmp://localhost/lang1/test\nrtmp://localhost/main/test_lang1_hi\n");
        return 0;
    }

    float bufTime = 0.0;
    float level1 = 0.5;
    float level2 = 0.5;

    if (argc > 6)
        level1 = atof(argv[6]);

    if (argc > 7)
        level2 = atof(argv[7]);

    if (argc > 8)
        bufTime = atof(argv[8]);

    // If we need ro write log into file - passing file name as #6 parameter
    if (argc > 9)
        ErrorHandler::instance()->SetLogFile(argv[9]);

    // AVlib common initialization
    av_register_all();
    avformat_network_init();
    //av_log_set_level(AV_LOG_DEBUG);
    av_log_set_level(AV_LOG_ERROR);

    Receiver receiver(argv[1], argv[2], argv[3], argv[4], argv[5], level1, level2, bufTime);

    receiver.startReadFrames();

    return 0;
}
